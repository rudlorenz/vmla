# Useful links

## C++ links

  1. [GCC debug Flags](http://codeforces.com/blog/entry/15547)
  2. [C++ perfomance tools](https://github.com/DigitalInBlue/Celero/)

## Task 1

  1. [Cubic Spline interpolation](http://fourier.eng.hmc.edu/e176/lectures/ch7/node6.html)
  2. [Численные методы решения СЛАУ](http://mathhelpplanet.com/static.php?p=chislennyye-metody-resheniya-slau)

## Task 2

  1. [Численное решение интегральных уравнений методом квадратур](http://num-anal.srcc.msu.ru/meth_mat/prac_qdf/ps/quadrpdf.pdf)