#ifndef TASK1_POINT_H
#define TASK1_POINT_H

#include <ostream>
#include <istream>

struct Point
{
    Point() : x_(0), y_(0) {};

    Point(double x, double y) : x_(x), y_(y) {};
    double x_;
    double y_;
};

std::ostream &operator<<(std::ostream &os, const Point &p)
{
    os << p.x_ << " " << p.y_ << "\n";
    return os;
}

std::istream &operator>>(std::istream &is, Point& p)
{
    is >> p.x_ >> p.y_;
    return is;
}

#endif //TASK1_POINT_H
