#ifndef TASK1_SPLINE_H
#define TASK1_SPLINE_H


#include <vector>
#include <cmath>
#include <iostream>
#include <algorithm>
#include "point.h"

std::vector<double> TridiagonalMatrix(const std::vector<double> &left,
                                      const std::vector<double> &center,
                                      const std::vector<double> &right,
                                      const std::vector<double> &r_side)
{
    auto N = left.size();

    std::vector<double> P, Q;
    P.reserve(N);
    Q.reserve(N);

    P.emplace_back(right[0] / -center[0]);
    Q.emplace_back(r_side[0] / center[0]);

    for (auto i = 1; i < N - 1; ++i)
    {
        P.emplace_back(right[i] / (-center[i] - left[i] * P[i - 1]));
        Q.emplace_back((left[i] * Q[i - 1] - r_side[i]) / (-center[i] - left[i] * P[i - 1]));
    }

    std::vector<double> x;
    x.reserve(N);

    x.emplace_back((left[N - 1] * Q[N - 2] - r_side[N - 1]) / (-center[N - 1] - left[N - 1] * P[N - 2]));

    for (auto i = 1; i < N; ++i) {
        x.emplace_back(P[N - i - 1] * x[i - 1] + Q[N - i - 1]);
    }

    return x;
}

//what a monstrosity
inline double SplineFunc(double x,
                         double xi, double xi1,
                         double yi, double yi1,
                         double Mi, double Mi1,
                         double hi)
{
    return std::pow(xi - x, 3) * Mi1 / (6 * hi) +
           std::pow(x - xi1, 3) * Mi / (6 * hi) +
           (yi1 / hi - (Mi1 * hi) / 6) * (xi - x) +
           (yi / hi - (Mi * hi) / 6) * (x - xi1);
}

double Substitute(double x,
                  const std::vector<Point> &vals,
                  const std::vector<double> &h,
                  const std::vector<double> &M
)
{
    auto N = vals.size();

    if (x <= vals[0].x_) {
        return SplineFunc(x, vals[1].x_, vals[0].x_, vals[1].y_, vals[0].y_, M[1], M[0], h[0]);
    }
    if (x >= vals.back().x_) {
        return SplineFunc(x, vals[N - 1].x_, vals[N - 2].x_, vals[N - 1].y_, vals[N - 2].y_, M[N - 1], M[N - 2], h[N - 2]);
    }

    int i = 1, j = N-1, mid;
    while (i < j)
    {
        mid = (i + j) / 2;
        if (x <= vals[mid].x_) {
            j = mid;
        }
        else {
            i = mid+1;
        }
    }

    return SplineFunc(x, vals[j].x_, vals[j - 1].x_, vals[j].y_, vals[j - 1].y_, M[j], M[j - 1], h[j - 1]);
}

std::vector<double> CalculateSplineCoefficients(const std::vector<Point> &values, const std::vector<double> &h,
                                                double LEFT_COND, double RIGHT_COND, int LEFT_COND_TYPE, int RIGHT_COND_TYPE)
{
    const int N = values.size();

    std::vector<double> left, center, right, r_side;
    left.reserve(N);
    center.reserve(N);
    right.reserve(N);
    r_side.reserve(N);

    left.emplace_back(0);

    if (LEFT_COND_TYPE == 1)
    {
        center.emplace_back(4);
        right.emplace_back(2);
        r_side.emplace_back(
                (12 / h[0]) * ((values[1].y_ - values[0].y_) / h[0] - LEFT_COND)
        );
    }
    else if (LEFT_COND_TYPE == 2)
    {
        center.emplace_back(2);
        right.emplace_back(0);
        r_side.emplace_back(2 * LEFT_COND);
    }
    else
    {
        std::cerr << "Left condition type error\n";
        std::exit(EXIT_FAILURE);
    }

    for (int i = 1; i < N - 1; ++i)
    {
        left.emplace_back(h[i - 1]);
        center.emplace_back(2 * (h[i] + h[i - 1]));
        right.emplace_back(h[i]);
        r_side.emplace_back(
                6 * (((values[i + 1].y_ - values[i].y_) / h[i]) - ((values[i].y_ - values[i - 1].y_) / h[i - 1]))
        );
    }

    right.emplace_back(0);

    if (RIGHT_COND_TYPE == 1)
    {
        left.emplace_back(1);
        center.emplace_back(2);
        r_side.emplace_back(6 / h[N - 2] * (RIGHT_COND - (values[N - 1].y_ - values[N - 2].y_) / h[N - 2]));
    }
    else if (RIGHT_COND_TYPE == 2)
    {
        left.emplace_back(0);
        center.emplace_back(1);
        r_side.emplace_back(RIGHT_COND);
    }
    else
    {
        std::cerr << "Right condition type error\n";
        std::exit(EXIT_FAILURE);
    }

    std::vector<double> M;
    M.reserve(N);

    //don't forget, that returned vector is reversed
    M = TridiagonalMatrix(left, center, right, r_side);

    std::reverse(M.begin(), M.end());

    return M;
}


#endif //TASK1_SPLINE_H
