#ifndef TASK1_FUNCT_H
#define TASK1_FUNCT_H

#include <cmath>

inline double Funct(double x)
{
    return std::abs(x+7);
    //return x * sin(2 * x + M_PI_4) + 1;
}

inline double DerivativeNumerical(double x)
{
    double eps = 0.000001;
    return 1 / eps * (Funct(x + eps) - Funct(x));
}

inline double DerivativeNumericalSecond(double x)
{
    double eps = 0.0000001;
    return 1 / (eps * eps) * (Funct(x) - 2.0 * Funct(x + eps) + Funct(x + 2.0 * eps));
}

#endif //TASK1_FUNCT_H