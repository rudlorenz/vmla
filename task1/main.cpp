#define _USE_MATH_DEFINES
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <sstream>
#include <iomanip>
#include <iterator>

#include "src/point.h"
#include "src/funct.h"
#include "src/spline.h"

//TESTED

int main()
{
    const double LEFT_COND = 1;
    const int LEFT_COND_TYPE = 1;
    const double RIGHT_COND = 15;
    const int RIGHT_COND_TYPE = 2;

    const double left_border = -10;
    const double right_border = -5;
    const int N = 4;
    const double dx = (right_border - left_border) / N;

    std::vector<Point> values;
    values.reserve(N);
    for (double i = left_border; i < right_border; i+=dx) {
        values.emplace_back(i, Funct(i));
    }

    for (auto &&item : values) {
        std::cout << item;
    }

    std::vector<double> h;
    h.reserve(N - 1);

    for (auto i = 1; i < N; ++i) {
        h.emplace_back(values[i].x_ - values[i - 1].x_);
    }

    std::vector<double> M = CalculateSplineCoefficients(values, h, LEFT_COND, RIGHT_COND, LEFT_COND_TYPE, RIGHT_COND_TYPE);


    std::stringstream buffer;
    std::stringstream tbuf;

    for (double i = values.front().x_; i <= values.back().x_; i += 0.01) {
        buffer << i << ' ' << Substitute(i, values, h, M) << ' ' << Funct(i) << '\n';
    }

    std::ofstream fout("data");
    if (!fout)
    {
        std::cerr << "Can't open output file. Abort\n";
        return -1;
    }

    fout << std::setprecision(5);
    fout << buffer.str();
    fout.close();

    std::cout << "Success! Now run 'gnuplot plot'\n";

    return 0;
}